// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

//
// function createNewUser() {
//     return {
//         firstName: prompt(`Enter your first name`),
//         lastName: prompt(`Enter your second name`),
//         getLogin() {
//             return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
//         }
//     };
// }
//
// let newUser = createNewUser();
// console.log(newUser);
// console.log(newUser.getLogin());
const createNewUser = () => {
    return {
        firstName: prompt(`Enter your first name`),
        lastName: prompt(`Enter your second name`),
        getLogin() {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        }
    };
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
